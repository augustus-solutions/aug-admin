import { Observable } from 'rxjs';
import { environment } from './../../environments/environment.prod';
import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Plugins } from '@capacitor/core'
const { Storage } = Plugins


@Injectable({
  providedIn: 'root'
})
export class CommonService {
  ///////////////////////////////////////////////////////////////////////////////////////
  // This http client will disable the interceptor and send non authenticated signals
  constructor(private http: HttpClient, handler: HttpBackend) {
    this.http = new HttpClient(handler);
  }
  ///////////////////////////////////////////////////////////////////////////////////////


  ///////////////////////////////////////////////////////////////////////////////////////
  private readonly APIurl = environment.APIurl + '/'
  private site_id: any
  private site: any = new Object()
  ///////////////////////////////////////////////////////////////////////////////////////



  ///////////////////////////////////////////////////////////////////////////////////////
  // Fetch Site ID for all Intercepts
  async get_site_id() {
    const { value } = await Storage.get({ key: 'site_id' });
    this.site_id = value
    this.site = { site_id: this.site_id }
  }
  ///////////////////////////////////////////////////////////////////////////////////////


  ///////////////////////////////////////////////////////////////////////////////////////
  // Basic Methods
  get_token() {
    try {
      return localStorage.getItem('token')
    } catch (error) {
      return Storage.get({ key: 'token' })
    }
  }
  ///////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // https://www.youtube.com/watch?v=U03PnQXpLVc&t=377s
  // 1. Copy this code to CommonService 2. Proxy.conf under src 3. Change onUpload Function
  // Image upload on ImgBB
  private readonly IBB_apikey: string = "3865e37f11a9a9b587d96a3d53fc5cfd"
  upload_image(file: File): Observable<any> {
    const formData = new FormData();
    formData.append('image', file)
    return this.http.post('https://api.imgbb.com/1/upload', formData, { params: { key: this.IBB_apikey } })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // Essentials
  login_with_creds(credentials: any): Observable<any> {
    return this.http.post<any>(this.APIurl + "auth/login/", credentials)
  }

  loggedIn() {
    try {
      return !!localStorage.getItem('token')
    } catch (error) {
      return !!Storage.get({ key: 'token' })
    }
  }

  check_names(data: any) {
    return this.http.post<any>(this.APIurl + "allaccount/", data)
  }

  /////////////////////////////////////////////////////////////////////////////////////////////



  ///////////////////////////////////////////////////////////////////////////////////////
  // Basic Lists Dump
  category_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/category-list/', { ...this.site })
  }

  sub_category_all_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/subcategory-list-all/', { ...this.site })
  }

  sub_category_detail(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/subcategory-detail/', { ...params, ...this.site })
  }

  sub_category_filter_list(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/subcategory-list-filter/', { ...params, ...this.site })
  }

  site_detail(): Observable<any> {
    return this.http.post(this.APIurl + 'core/site-detail/', { ...this.site })
  }
  ///////////////////////////////////////////////////////////////////////////////////////


}
