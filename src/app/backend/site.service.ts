import { logging } from 'protractor';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class SiteService {

  constructor(private http: HttpClient) { }
  private readonly APIurl = environment.APIurl + '/'
  private site_id: any
  private site: any = new Object()

  // Fetch Site ID for all Intercepts
  async get_site_id() {
    const { value } = await Storage.get({ key: 'site_id' });
    this.site_id = value
    this.site = { site_id: this.site_id }
  }



  ///////////////////////////////////////////////////////////////////////////////////////
  // Basic Lists Dump
  site_detail(): Observable<any> {
    return this.http.post(this.APIurl + 'core/site-detail/', { ...this.site })
  }
  ///////////////////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Category CRUD [ visitor ]
  visitor_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'auth/visitor-cud/', { ...params, ...{ site: this.site_id } })
  }

  visitor_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'auth/visitor-cud/', params)
  }

  visitor_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/visitor-list/', { ...this.site })
  }

  visitor_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        site_id: this.site_id,
        visitor_id: params.visitor_id
      },
    };

    return this.http.delete(this.APIurl + 'auth/visitor-cud/', options)
  }

  //////////////////////////////////////////////////////////////////////////



  //////////////////////////////////////////////////////////////////////////
  // Category CRUD
  category_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/category-cud/', params)
  }

  category_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/category-cud/', params)
  }

  category_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/category-list/', { ...this.site })
  }

  category_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        site_id: this.site_id,
        category_id: params.category_id
      },
    };

    return this.http.delete(this.APIurl + 'core/category-cud/', options)
  }

  //////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////
  // Subcategory CRUD
  sub_category_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/subcategory-cud/', params)
  }

  sub_category_edit(params: any): Observable<any> {
    console.log(params);

    return this.http.put(this.APIurl + 'core/subcategory-cud/', params)
  }

  sub_category_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        site_id: this.site_id,
        sub_category_id: params.sub_category_id
      },
    };
    return this.http.delete(this.APIurl + 'core/subcategory-cud/', options)
  }

  sub_category_all_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/subcategory-list-all/', { ...this.site })
  }

  sub_category_filter_list(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/subcategory-list-filter/', { ...params, ...this.site })
  }


  //////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////
  // Subcategory CRUD
  post_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/post-cud/', { ...params, ...{ site: this.site_id } })
  }

  post_edit(params: any): Observable<any> {
    return this.http.put(this.APIurl + 'core/post-cud/', params)
  }

  post_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        site_id: this.site_id,
        post_id: params.post_id
      },
    };
    return this.http.delete(this.APIurl + 'core/post-cud/', options)
  }

  post_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/post-list/', { ...this.site })
  }

  post_checker(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/post-checker/', { ...params, ...this.site })
  }


  //////////////////////////////////////////////////////////////////////////


  //////////////////////////////////////////////////////////////////////////
  // Gallery CRUD [ gallery ]
  gallery_create(params: any): Observable<any> {
    return this.http.post(this.APIurl + 'core/gallery-cud/', params)
  }

  gallery_edit(params: any): Observable<any> {
    console.log(params);

    return this.http.put(this.APIurl + 'core/gallery-cud/', params)
  }

  gallery_delete(params: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        site_id: this.site_id,
        gallery_image_id: params.gallery_image_id
      },
    };
    console.log(options);

    return this.http.delete(this.APIurl + 'core/gallery-cud/', options)
  }

  gallery_list(): Observable<any> {
    return this.http.post(this.APIurl + 'core/gallery-list/', { ...this.site })
  }

  //////////////////////////////////////////////////////////////////////////



  //////////////////////////////////////////////////////////////////////////
  // Visitor
  customer_list(): Observable<any> {
    return this.http.post(this.APIurl + 'auth/visitor-list/', { ...this.site })
  }
  //////////////////////////////////////////////////////////////////////////

}
