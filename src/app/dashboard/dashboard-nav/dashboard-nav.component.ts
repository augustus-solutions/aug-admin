import { SiteService } from './../../backend/site.service';
import { CommonService } from './../../backend/common.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-dashboard-nav',
  templateUrl: './dashboard-nav.component.html',
  styleUrls: ['./dashboard-nav.component.scss']
})
export class DashboardNavComponent implements OnInit {

  constructor(private router: Router, private commonService: CommonService, private siteService: SiteService) { }

  ngOnInit() {
    this.detail_shop()
  }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  detail_shop() {
    this.siteService.site_detail().subscribe(
      resp => {
        this.Shop = resp
        console.log(resp);
        Storage.set({ key: 'site_id', value: resp['site_id'] });
        this.commonService.get_site_id()
        this.siteService.get_site_id()
        $(".preloader").fadeOut();
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  prviousNav = 1
  toValhalla(nav) {
    var element = $('ul#sidebarnav a')
    element.parentsUntil(".sidebar-nav").each(function (index) {
      if ($(this).is("li") && $(this).children("a").length !== 0) {
        $(this).children("a").addClass("active");
        $(this).parent("ul#sidebarnav").length === 0 ?
          $(this).removeClass("active") :
          $(this).removeClass("selected");
      } else if (!$(this).is("ul") && $(this).children("a").length === 0) {
        $(this).removeClass("selected");

      } else if ($(this).is("ul")) {
        $(this).removeClass('in');
      }
    });
    $("#main-wrapper").addClass("show-sidebar");
    $(".nav-toggler i").addClass("ti-menu");

    $(".nav" + nav).addClass("selected");
    $(".nav" + this.prviousNav).removeClass("selected");
    if (this.prviousNav == nav) {
      this.prviousNav = nav
    }
    // this.router.navigate([route])
  }





}


