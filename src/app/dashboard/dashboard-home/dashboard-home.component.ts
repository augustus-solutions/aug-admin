import { environment } from './../../../environments/environment.prod';
import { NgForm } from '@angular/forms';
import { SiteService } from 'src/app/backend/site.service';
import { CommonService } from './../../backend/common.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatePickerDirective } from 'ng2-date-picker';
import { MatAccordion } from '@angular/material/expansion';
declare var $: any;

import { Plugins } from '@capacitor/core';
const { Storage } = Plugins;
@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.scss']
})
export class DashboardHomeComponent implements OnInit {

  constructor(private router: Router, private commonService: CommonService, private siteService: SiteService) { }
  @ViewChild(MatAccordion) accordion: MatAccordion;

  @ViewChild('dateDirectivePicker')
  datePickerDirective: DatePickerDirective;
  selectedDate: any;
  config: any;

  ngOnInit() {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.detail_shop()
    this.available_times = []
    this.add_years(this.max_today, 5);
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  active_day = new Date();
  selected_date = new Date();
  isLoading = false
  isExpand = false
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  onDateChange(e) {
    console.log(this.active_day);
    this.active_day = e.target.value
    this.Appoitments = []
    this.isLoading = true
    $(".preloader").fadeIn();
    this.list_appointments()
  }

  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }

  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Timer Functions
  // Input : Raw DateTime // Output : dd-mm-yy
  DateTimeToDay(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }
  /////////////////////////////////////////////////////////////////////////////////////////////




  sc_id_str = '';
  async loadScID() {
    const sc_id = await Storage.get({ key: 'sub_category_id' })
    if (sc_id && sc_id.value) {
      console.log('set sc_id: ', sc_id.value);
      this.sc_id_str = sc_id.value;
    }
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Date Operations
  today = new Date()
  max_today = new Date()
  minSelectabledate = this.formatDateforPicker(this.today);
  maxSelectabledate = this.formatDateforPicker(this.max_today);
  /////////////////////////////////////////////////////////////////////////////////////////////
  add_years(dt, n) {
    return new Date(dt.setFullYear(dt.getFullYear() + n));
  }
  formatDateforPicker(date) {

    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    // console.log("day", day);
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
  formatDateforPicker2(date) {

    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + (d.getDate() - 1),
      year = d.getFullYear();

    console.log("day", day);
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }
  getTime(value) {
    var ISTOffset = 330;   // IST offset UTC +5:30
    var temp = new Date(this.minSelectabledate + 'T' + value + 'Z');
    temp = new Date(temp.getTime() - (ISTOffset) * 60000);
    return temp
  }
  getDateTime(value) {
    var ISTOffset = 330;   // IST offset UTC +5:30
    var temp = new Date(value);
    temp = new Date(temp.getTime() - (ISTOffset) * 60000);
    return temp
  }
  formatTime(time) {
    var d = new Date(time),
      minutes = '' + (d.getMinutes()),
      hours = '' + d.getHours()

    if (minutes.length < 2) minutes = '0' + minutes;
    if (hours.length < 2) hours = '0' + hours;

    return [hours, minutes].join(':');
  }
  addTime(time) {
    var local = time
    return new Date(local.setMinutes(local.getMinutes() + this.Shop.avg_appoinment_time));
  }
  toBackend(time) {
    var sle_date = new Date(this.selected_date),
      month = '' + (sle_date.getMonth() + 1),
      day = '' + (sle_date.getDate()),
      year = sle_date.getFullYear()
    var d = new Date(time),
      minutes = '' + (d.getMinutes()),
      hours = '' + d.getHours()

    if (minutes.length < 2) minutes = '0' + minutes;
    if (hours.length < 2) hours = '0' + hours;

    var times = [hours, minutes].join(':');
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var date = [year, month, day].join('-');

    return [date, times].join('T')

  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  available_times: any = []
  map_appoitments() {
    this.isLoading = false
    // var currentOffset = open_time.getTimezoneOffset();
    var open_time = this.getTime(this.Shop.open_time)
    var close_time = this.getTime(this.Shop.close_time)
    var curr_time = this.getTime(this.Shop.open_time)

    while (curr_time.getTime() <= close_time.getTime()) {
      var i
      var temp_time = curr_time.toString()
      this.available_times.push({ 'time': this.formatTime(curr_time), 'value': temp_time })
      curr_time = this.addTime(curr_time)
    }
    for (let i = 0; i < this.Appoitments.length; i++) {
      const appointment_time = this.getDateTime(this.Appoitments[i].due_or_spec_time);
      console.log(appointment_time);

      this.available_times = this.available_times.filter(item => this.formatTime(appointment_time) != item.time)
      // if (curr_time.getTime() != appointment_time.getTime()) {
      // }
    }
    console.log(this.available_times);
    this.selected_time = this.available_times[0].value
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  selected_time: any;
  onSeatClick(factor) {
    this.selected_time = factor.value
  }

  Post: any = new Object()
  /////////////////////////////////////////////////////////////////////////////////////////////
  SubCategory: any = new Object();
  APIurl = environment.APIurl
  Creating_Factor: any
  comment: any = ''
  book_form(form: NgForm) {
    this.Post.comments = this.comment
    this.Post.subcategory = this.selectedSubCategory
    this.Post.customer = this.selectedCustomer
    this.Post.due_or_spec_time = this.toBackend(this.selected_time)
    console.log(this.Post);
    this.siteService.post_create(this.Post).subscribe(
      resp => {
        console.log(resp);
        this.router.navigate([''])
        $('#create-factor-modal').modal('hide');
        this.selected_date = new Date();
        this.list_appointments()
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Appointment List // Appointments

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  message: any = new Object();
  Visitors: any = [];
  list_visitors() {
    this.siteService.visitor_list().subscribe(
      resp => {
        this.Visitors = resp
        console.log(resp);
      })
  }
  SubCatgories: any = [];
  list_sub_categories() {
    this.siteService.sub_category_all_list().subscribe(
      resp => {
        this.SubCatgories = resp
        console.log(resp);
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  Appoitments: any = [];
  flag = 0
  list_appointments() {
    if (this.flag == 0) {
      var selected_data_formatted = this.formatDateforPicker(this.selected_date);
    } else {
      var selected_data_formatted = this.formatDateforPicker(this.selected_date);
    }

    this.SubCategory.due_or_spec_time = selected_data_formatted
    console.log(this.SubCategory);

    this.siteService.post_checker(this.SubCategory).subscribe(
      resp => {
        this.Appoitments = resp
        console.log(resp);
        this.map_appoitments()
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Shop Detail // Shop
  Shop: any;
  detail_shop() {
    this.siteService.site_detail().subscribe(
      resp => {
        this.Shop = resp
        console.log(resp);
        this.list_appointments()
        this.list_sub_categories()
        this.list_visitors()
      })
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  reset_mapping(event) {
    // console.log(event);
    this.selected_date = event.target.value
    console.log(this.selected_date);
    this.flag = 1
    this.available_times = []
    this.detail_shop()
    this.isLoading = true
  }
  // this.ngOnInit()

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ visitor -  ]
  Editing_Factor: any = new Object()
  delete_factor(ele: any) {
    this.Editing_Factor.post_id = ele.post_id
    this.siteService.post_delete(this.Editing_Factor).subscribe(
      resp => {
        this.list_appointments()
        $('#edit-factor-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Misc Functions
  selectedCustomer: any
  onSelect(e) {
    console.log(e);
    this.selectedCustomer = e.visitor_id
    console.log(this.selectedCustomer);
  }
  selectedSubCategory: any
  onSelect2(e) {
    console.log(e);
    this.selectedSubCategory = e.sub_category_id
    console.log(this.selectedSubCategory);
  }

  /////////////////////////////////////////////////////////////////////////////////////////////


}
