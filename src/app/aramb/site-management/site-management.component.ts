import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SiteService } from './../../backend/site.service';
import { Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-site-management',
  templateUrl: './site-management.component.html',
  styleUrls: ['./site-management.component.scss']
})
export class SiteManagementComponent implements OnInit {

  constructor(private siteService: SiteService, private router: Router) { }

  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.detail_shop()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  Shop: any;
  detail_shop() {
    this.siteService.site_detail().subscribe(
      resp => {
        this.Shop = resp
        $(".preloader").fadeOut();
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  selectedImage: File = null;
  onImage(event) {
    this.selectedImage = <File>event.target.files[0];
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category
  edit_factor(form: NgForm) {
    const uploadData = new FormData();
    uploadData.append('category_id', form.value.category_id)
    uploadData.append('category_name', form.value.category_name)
    if (this.selectedImage != null) {
      uploadData.append('category_cover', this.selectedImage, this.selectedImage.name)
    }

    this.siteService.category_edit(uploadData).subscribe(
      resp => {
        console.log(resp)
        // this.router.navigate(['/dashboard/cat-management'])
      },
      error => {
        console.log('error', error);
      }
    );
  }

  /////////////////////////////////////////////////////////////////////////////////////////////



}
