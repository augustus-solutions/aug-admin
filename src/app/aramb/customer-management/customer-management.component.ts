import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SiteService } from './../../backend/site.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;
@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.scss']
})
export class CustomerManagementComponent implements OnInit {

  constructor(private siteService: SiteService, private router: Router,) { }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // DataTable Imports // Before Construtor
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};
  /////////////////////////////////////////////////////////////////////////////////////////////

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  message: any = new Object();
  Factors: any = [];
  list_factor() {
    this.siteService.visitor_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()

      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ visitor ]
  Creating_Factor: any = new Object()
  create_factor(form: NgForm) {
    this.Creating_Factor.email = form.value.customer_mobile
    this.Creating_Factor.password = form.value.customer_mobile

    // console.log(this.Creating_Factor);
    this.siteService.visitor_create(this.Creating_Factor).subscribe(
      resp => {
        this.Creating_Factor.visitor_id = resp.visitor_id
        console.log(this.Creating_Factor);

        console.log(resp.customer_id)
        this.siteService.visitor_edit(this.Creating_Factor).subscribe(
          resp => {
            console.log(resp);

            this.list_factor()
            this.Creating_Factor = new Object()
            $('#create-factor-modal').modal('hide');
          }
        )
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ visitor ]
  Editing_Factor: any = new Object()

  edit_factor(form: NgForm) {

    this.siteService.visitor_edit(this.Editing_Factor).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Editing_Factor = new Object()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ visitor -  ]
  delete_factor(ele: any) {
    this.Editing_Factor.visitor_id = ele.visitor_id
    this.siteService.visitor_delete(this.Editing_Factor).subscribe(
      resp => {
        this.list_factor()
        $('#edit-factor-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

  show_edit(element: any) {
    console.log(element);
    this.Editing_Factor = element
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



  /////////////////////////////////////////////////////////////////////////////////////////////
  // DataTables Functions // Functions at last
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }


  // Put this funtion inside the table fetcher
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
