import { CommonService } from './../../backend/common.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SiteService } from './../../backend/site.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;

@Component({
  selector: 'app-gallery-management',
  templateUrl: './gallery-management.component.html',
  styleUrls: ['./gallery-management.component.scss']
})
export class GalleryManagementComponent implements OnInit {

  constructor(private siteService: SiteService, private router: Router, private commonService: CommonService) { }

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    this.list_sub_category()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  message: any = new Object();
  selectedSubCategory: any;
  SubCategories: any = [];
  list_sub_category() {
    this.siteService.sub_category_all_list().subscribe(
      resp => {
        this.SubCategories = resp
        console.log(resp);
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  Factors: any = [];
  list_factor() {
    this.siteService.gallery_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()

      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  delete_factor(element: any) {

    this.Editing_Factor.gallery_image_id = element.gallery_image_id
    this.siteService.gallery_delete(this.Editing_Factor).subscribe(
      resp => {
        this.list_factor()
      },
    )
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  selectedImage: File = null;
  selectedImageURL: any = null;
  isUploading: boolean = false
  onImage(event) {
    this.selectedImage = <File>event.target.files[0];
    this.isUploading = true
    this.commonService.upload_image(this.selectedImage).subscribe(
      resp => {
        console.log(resp);
        this.selectedImageURL = resp['data']['url']
        if (resp['status'] == 200) {
          this.isUploading = false
          console.log(this.selectedImageURL);
        }
      }
    )
  }
  //   site: {{ site_id }}
  // category: { { category_id } }
  // sc_name: Men's Haircut
  // sc_description: Lorem Ipsum Lorem Ipsum Lorem Ipsum
  // sc_actual_price:
  // sc_discounted_price:
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Create Category
  Creating_Factor: any = new Object()

  create_factor(form: NgForm) {
    const uploadData = new FormData();
    console.log(this.selectedSubCategory);

    uploadData.append('sub_category', this.selectedSubCategory)
    uploadData.append('image_name', form.value.image_name)
    if (this.selectedImage != null) {
      uploadData.append('gallery_image', this.selectedImageURL)
    }

    this.siteService.gallery_create(uploadData).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Creating_Factor = new Object()
        this.selectedSubCategory = null
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category
  Editing_Factor: any = new Object()

  edit_factor(form: NgForm) {
    const uploadDataE = new FormData();

    console.log(this.Editing_Factor);

    uploadDataE.append('gallery_image_id', this.Editing_Factor.gallery_image_id)
    // uploadDataE.append('sub_category', this.selectedSubCategory)
    uploadDataE.append('image_name', form.value.image_name)
    if (this.selectedImage != null) {
      uploadDataE.append('gallery_image', this.selectedImageURL)
    }


    this.siteService.gallery_edit(uploadDataE).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Editing_Factor = new Object()
        // this.selectedSubCategory = null
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

  print(e) {
    console.log(e);

    this.selectedSubCategory = e.sub_category_id
    console.log(this.selectedSubCategory);

  }

  show_edit(element: any) {
    this.selectedSubCategory = element
    this.Editing_Factor = element
    console.log(element);
    $('#edit-factor-modal').modal('show')
    // var edit_modal = document.getElementById('edit-factor-modal')
    // edit_modal.modal(show)

  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};
  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////



}
