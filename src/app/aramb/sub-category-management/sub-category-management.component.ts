import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SiteService } from './../../backend/site.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;
@Component({
  selector: 'app-sub-category-management',
  templateUrl: './sub-category-management.component.html',
  styleUrls: ['./sub-category-management.component.scss']
})
export class SubCategoryManagementComponent implements OnInit {

  constructor(private siteService: SiteService, private router: Router) { }

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    this.list_category()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  message: any = new Object();
  selectedCategory: any;
  Categories: any = [];
  list_category() {
    this.siteService.category_list().subscribe(
      resp => {
        this.Categories = resp
        console.log(resp);
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  Factors: any = [];
  list_factor() {
    this.siteService.sub_category_all_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()

      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  delete_factor(element: any) {

    this.Editing_Factor.sub_category_id = element.sub_category_id
    this.siteService.sub_category_delete(this.Editing_Factor).subscribe(
      resp => {
        this.list_factor()
      },
    )
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  selectedImage: File = null;
  onImage(event) {
    this.selectedImage = <File>event.target.files[0];
  }
  //   site: {{ site_id }}
  // category: { { category_id } }
  // sc_name: Men's Haircut
  // sc_description: Lorem Ipsum Lorem Ipsum Lorem Ipsum
  // sc_actual_price:
  // sc_discounted_price:
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Create Category
  Creating_Factor: any = new Object()

  create_factor(form: NgForm) {
    const uploadData = new FormData();
    console.log(this.selectedCategory);

    uploadData.append('category', this.selectedCategory)
    uploadData.append('sc_name', form.value.sc_name)
    uploadData.append('sc_description', form.value.sc_description)
    console.log(form.value);
    if (form.value.sc_actual_price == undefined) {
      uploadData.append('sc_actual_price', '0')
    } else {
      uploadData.append('sc_actual_price', form.value.sc_actual_price)
    }
    if (form.value.sc_discounted_price == undefined) {
      uploadData.append('sc_discounted_price', '0')
    } else {
      uploadData.append('sc_discounted_price', form.value.sc_discounted_price)
    }
    if (this.selectedImage != null) {
      uploadData.append('sc_cover', this.selectedImage, this.selectedImage.name)
    }

    this.siteService.sub_category_create(uploadData).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Creating_Factor = new Object()
        this.selectedCategory = null
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category
  Editing_Factor: any = new Object()

  edit_factor(form: NgForm) {
    const uploadDataE = new FormData();
    uploadDataE.append('category', this.Editing_Factor.category_id)
    uploadDataE.append('sub_category_id', this.Editing_Factor.sub_category_id)
    uploadDataE.append('sc_name', form.value.sc_name)
    uploadDataE.append('sc_description', form.value.sc_description)

    if (form.value.sc_actual_price == undefined) {
      uploadDataE.append('sc_actual_price', '0')
    } else {
      uploadDataE.append('sc_actual_price', form.value.sc_actual_price)
    }
    if (form.value.sc_discounted_price == undefined) {
      uploadDataE.append('sc_discounted_price', '0')
    } else {
      uploadDataE.append('sc_discounted_price', form.value.sc_discounted_price)
    }
    if (this.selectedImage != null) {
      uploadDataE.append('sc_cover', this.selectedImage, this.selectedImage.name)
    }


    this.siteService.sub_category_edit(uploadDataE).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Editing_Factor = new Object()
        this.selectedCategory = null
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

  print(e) {
    console.log(e);

    this.selectedCategory = e.category_id
    console.log(this.selectedCategory);

  }

  show_edit(element: any) {
    this.selectedCategory = element
    this.Editing_Factor = element
    console.log(element);
    $('#edit-factor-modal').modal('show')
    // var edit_modal = document.getElementById('edit-factor-modal')
    // edit_modal.modal(show)

  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};
  // DataTables Functions
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


}
