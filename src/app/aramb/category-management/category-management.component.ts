import { CommonService } from './../../backend/common.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SiteService } from './../../backend/site.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { MatAccordion } from '@angular/material/expansion';
declare var $: any;

// @ViewChild(DataTableDirective, { static: false })

@Component({
  selector: 'app-category-management',
  templateUrl: './category-management.component.html',
  styleUrls: ['./category-management.component.scss']
})
export class CategoryManagementComponent implements OnInit {

  constructor(private siteService: SiteService, private router: Router, private commonService: CommonService) { }

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    this.list_sub_category()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  message: any = new Object();
  Factors: any = [];
  list_factor() {
    this.siteService.category_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        // this.rerender()
        // this.dtTrigger.next();
        $(".preloader").fadeOut();
      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  Factor: any = new Object()
  delete_factor(element: any) {

    this.Factor.category_id = element.category_id
    this.siteService.category_delete(this.Factor).subscribe(
      resp => {
        this.list_factor()
      },
    )
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  selectedImage: File = null;
  selectedImageURL: any = null;
  isUploading: boolean = false
  onImage(event) {
    this.selectedImage = <File>event.target.files[0];
    this.isUploading = true
    this.commonService.upload_image(this.selectedImage).subscribe(
      resp => {
        console.log(resp);
        this.selectedImageURL = resp['data']['url']
        if (resp['status'] == 200) {
          this.isUploading = false
          console.log(this.selectedImageURL);
        }
      }
    )
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Create Category
  create_factor(form: NgForm) {
    const uploadData = new FormData();
    uploadData.append('category_name', form.value.category_name)
    uploadData.append('category_description', form.value.category_description)
    if (this.selectedImage != null) {
      uploadData.append('category_cover', this.selectedImageURL)
    }

    this.siteService.category_create(uploadData).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#create-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category
  edit_factor(form: NgForm) {
    const uploadData = new FormData();
    uploadData.append('category_id', this.Factor.category_id)
    uploadData.append('category_name', form.value.category_name)
    uploadData.append('category_description', form.value.category_description)
    if (this.selectedImage != null) {
      uploadData.append('category_cover', this.selectedImageURL)
    }

    this.siteService.category_edit(uploadData).subscribe(
      resp => {
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

  show_edit(element: any) {
    console.log(element);
    this.Factor = element
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  isExpand: any = false
  @ViewChild(MatAccordion) accordion: MatAccordion;
  // dtEle: DataTableDirective;
  // dtTrigger: Subject<any> = new Subject<any>();
  // dtOptions: DataTables.Settings = {};

  // // DataTables Functions
  // ngAfterViewInit(): void {
  //   this.dtTrigger.next();
  // }

  // ngOnDestroy(): void {
  //   // Do not forget to unsubscribe the event
  //   this.dtTrigger.unsubscribe();
  // }

  // rerender(): void {
  //   this.dtEle.dtInstance.then((dtInstance: DataTables.Api) => {
  //     // Destroy the table first
  //     dtInstance.destroy();
  //     // Call the dtTrigger to rerender again
  //     this.dtTrigger.next();
  //   });
  // }
  togglePanels() {
    if (this.isExpand) { this.accordion.closeAll() }
    else { this.accordion.openAll() }
    this.isExpand = !this.isExpand
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  SubCategories: any = [];
  selectedCategory: any;
  list_sub_category() {
    this.siteService.sub_category_all_list().subscribe(
      resp => {
        this.SubCategories = resp
        console.log(resp);
        // this.rerender()

      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  delete_sub_category(element: any) {

    this.Editing_sub_Factor.sub_category_id = element.sub_category_id
    this.siteService.sub_category_delete(this.Editing_sub_Factor).subscribe(
      resp => {
        this.list_sub_category()
      },
    )
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Create Category
  Creating_sub_Factor: any = new Object()

  add_sub_category(factor: any) {
    this.selectedCategory = factor.category_id
    $('#create-sub-factor-modal').modal('show')

  }
  create_sub_category(form: NgForm) {
    const uploadData = new FormData();
    console.log(this.selectedCategory);

    uploadData.append('category', this.selectedCategory)
    uploadData.append('sc_name', form.value.sc_name)
    uploadData.append('sc_description', form.value.sc_description)
    console.log(form.value);
    if (form.value.sc_actual_price == undefined) {
      uploadData.append('sc_actual_price', '0')
    } else {
      uploadData.append('sc_actual_price', form.value.sc_actual_price)
    }
    if (form.value.sc_discounted_price == undefined) {
      uploadData.append('sc_discounted_price', '0')
    } else {
      uploadData.append('sc_discounted_price', form.value.sc_discounted_price)
    }
    if (this.selectedImage != null) {
      uploadData.append('sc_cover', this.selectedImageURL)
    }

    this.siteService.sub_category_create(uploadData).subscribe(
      resp => {
        console.log(resp)
        this.list_sub_category()
        this.Creating_sub_Factor = new Object()
        this.selectedCategory = null
        $('#create-sub-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category
  Editing_sub_Factor: any = new Object()
  SubFactor: any = new Object()
  show_sub_edit(element: any) {
    console.log(element);
    this.Editing_sub_Factor = element
    $('#edit-sub-factor-modal').modal('show')
  }

  edit_sub_category(form: NgForm) {
    const uploadDataE = new FormData();
    uploadDataE.append('category', this.Editing_sub_Factor.category_id)
    uploadDataE.append('sub_category_id', this.Editing_sub_Factor.sub_category_id)
    uploadDataE.append('sc_name', form.value.sc_name)
    uploadDataE.append('sc_description', form.value.sc_description)

    if (form.value.sc_actual_price == undefined) {
      uploadDataE.append('sc_actual_price', '0')
    } else {
      uploadDataE.append('sc_actual_price', form.value.sc_actual_price)
    }
    if (form.value.sc_discounted_price == undefined) {
      uploadDataE.append('sc_discounted_price', '0')
    } else {
      uploadDataE.append('sc_discounted_price', form.value.sc_discounted_price)
    }
    if (this.selectedImage != null) {
      uploadDataE.append('sc_cover', this.selectedImageURL)
    }


    this.siteService.sub_category_edit(uploadDataE).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_sub_category()
        this.Editing_sub_Factor = new Object()
        this.selectedCategory = null
        $('#edit-factor-modal').modal('hide');
      },
      error => {
        console.log('error', error);
      }
    );
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

}
